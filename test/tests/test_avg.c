#include "integer_avg.h"
#include "unity.h"

#include <stdlib.h>
#include <string.h>
#include <limits.h>


void setUp(void)
{
    /* Set stuff up here */
}

void tearDown(void)
{
    /* Clean stuff up here */
}

void test_avg_function(void)
{
    /* Check result */
    TEST_ASSERT_EQUAL_INT_MESSAGE(6, integer_avg(4,8), "Error in integer_avg");
    TEST_ASSERT_EQUAL_INT_MESSAGE(3, integer_avg(2,4), "Error in integer_avg");
    TEST_ASSERT_EQUAL_INT_MESSAGE(4, integer_avg(4,5), "Error in integer_avg");
    TEST_ASSERT_EQUAL_INT_MESSAGE(0, integer_avg(0,1), "Error in integer_avg");
    TEST_ASSERT_EQUAL_INT_MESSAGE(0, integer_avg(0,-1), "Error in integer_avg");
    TEST_ASSERT_EQUAL_INT_MESSAGE(0, integer_avg(2,-2), "Error in integer_avg");
}

