#ifndef __INTEGER_AVG_H
#define __INTEGER_AVG_H

/** @return The sum of two integers @ref a and @ref b. */
int integer_avg(int a, int b);

#endif //__INTEGER_AVG_H